# ideas

## How I want to describe input data set

comp=[1,2,3]

gt2=[x if x>2]

gt2lt10=[x for x in gt2 && x<10]

gt2 == intersect(gt2, gt2lt10)

GT2(x) = [if x>2] or {if x>2}


- for でいいのかifでいいのか
- forは列挙するとき(comprehension)
- if は条件で順番関係ないとき


だととすると

```
[x for x in A && x in B]
&&がintersectだとすると
[x for x in A && B]
と同値
```

とすると記法としては

```
{x if x>2}
{x for x>2 && x<10}

[if x>2]
[for x>2 && x<10}]

```

のように、述語と集合を分けて書いたほうがいいかも。

集合は{}と書きたい
条件は[]ではなさそう

Juliaだと{}は型のパラメタか・・・
[]は配列
()はくくり
<>は比較演算子二個


